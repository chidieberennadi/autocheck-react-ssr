import { useState, useEffect, useCallback } from 'react';
import type { NextPage } from 'next';
import { FaTruckLoading, FaShippingFast, FaThumbsUp } from 'react-icons/fa';

import { AppLayout, ProductGrid, SingleProduct } from '../components/layout';
import { SideBar } from '../components/sidebar';
import { CarData, getPopularMakes, listCars, MakeData } from '../lib/autocheck';

const PageMontage = () => {
  return (
    <section className="bg-[url('/img/1_rangerover_tracking.jpeg')] bg-cover">
      <div className='container mx-auto'>
        <div className='h-screen flex items-center'>
          <div className='bg-opacity-20 bg-black w-full p-6'>
            <h3 className='text-2xl text-white mb-2'>Want to buy a car?</h3>
            <h1 className='text-5xl text-white mb-6 font-bold'>
              Find the right car to buy
            </h1>
            <button className='bg-sky-600 text-white px-4 h-10'>
              Shop Now
            </button>
          </div>
        </div>
      </div>
    </section>
  );
};

const SpecialOffer = () => {
  return <></>;
};

const FooterProductAds = () => {
  return (
    <section className="bg-[url('/img/9_2021_toyota_avalon.jpeg')] bg-cover bg-center py-16">
      <div className='container mx-auto'>
        <div className='grid grid-cols-2 gap-4'>
          <div className='bg-white bg-opacity-75 p-8'>
            <p className='text-orange-600 text-lg'>
              Smooth, Rich and Loud Audio
            </p>
            <h4 className='text-2xl mb-3'>Branded Headphones</h4>
            <small className='text-gray-500'>
              Sale up to 20% off all in store
            </small>
          </div>
          <div className='bg-white bg-opacity-75 p-8'>
            <p className='text-orange-600 text-lg'>
              Smooth, Rich and Loud Audio
            </p>
            <h4 className='text-2xl mb-3'>Branded Headphones</h4>
            <small className='text-gray-500'>
              Sale up to 20% off all in store
            </small>
          </div>
        </div>
      </div>
    </section>
  );
};

const FooterUSP = () => {
  return (
    <section className='py-16'>
      <div className='container mx-auto'>
        <article className='mb-8'>
          <h4 className='text-lg font-semibold mb-2'>Electronics</h4>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi minus
            illo atque ex quod laborum rem accusamus architecto, molestias
            aperiam! Labore quae laudantium voluptatum consequatur iusto fugit
            in expedita repudiandae?
          </p>
        </article>
        <div className='grid grid-cols-3 gap-4 border-t border-b py-6 '>
          <div className='flex items-center justify-center'>
            <FaTruckLoading className='w-8 h-8 text-sky-600' />
            <div className='ml-6'>
              <h4 className='text-xl font-semibold'>Free Shipping</h4>
              <p>on orders over $100</p>
            </div>
          </div>
          <div className='flex items-center justify-center'>
            <FaShippingFast className='w-8 h-8 text-sky-600' />
            <div className='ml-6'>
              <h4 className='text-xl font-semibold'>Fast Delivery</h4>
              <p>worldwide</p>
            </div>
          </div>
          <div className='flex items-center justify-center'>
            <FaThumbsUp className='w-8 h-8 text-sky-600' />
            <div className='ml-6'>
              <h4 className='text-xl font-semibold'>Big Choice</h4>
              <p>of products</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

const Home: NextPage = () => {
  const [popularCarData, setPopularCarData] = useState<
    { make: MakeData; cars: CarData[] }[]
  >([]);

  const loadCarData = useCallback(async () => {
    const allPopularMakes = await getPopularMakes();
    const allPopularCars = await Promise.all(
      allPopularMakes.slice(0, 4).map(async (make) => ({
        make,
        cars: (await listCars({ makeId: make.id })).slice(0, 6),
      })),
    );
    setPopularCarData(allPopularCars);
  }, []);

  useEffect(() => {
    loadCarData();
  }, []);

  return (
    <AppLayout>
      <PageMontage />
      <main className='py-16'>
        <div className='container mx-auto'>
          <h2 className='text-center text-3xl font-bold pb-16'>
            Our New Products
          </h2>
          <div className='grid grid-cols-4 gap-4'>
            <div className='col-span-3'>
              {popularCarData.map((item) => (
                <ProductGrid title={item.make.name} key={item.make.id}>
                  {item.cars.map((car) => (
                    <SingleProduct
                      key={car.id}
                      image={car.imageUrl}
                      productName={car.title}
                      productPrice={car.marketplacePrice}
                      productPreviousPrice={car.marketplaceOldPrice}
                      linkUrl={`/cars/${car.id}`}
                    />
                  ))}
                </ProductGrid>
              ))}
              <SpecialOffer />
            </div>
            <div>
              <SideBar />
            </div>
          </div>
        </div>
      </main>
      <FooterProductAds />
      <FooterUSP />
    </AppLayout>
  );
};

export default Home;
