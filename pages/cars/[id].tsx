import { useCallback, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Image from 'next/image';
import { FaTachometerAlt, FaMapMarkedAlt } from 'react-icons/fa';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';

import {
  CarDetails,
  CarMedia,
  formatCurrency,
  formatNumber,
  getCarData,
  getCarMedia,
} from '../../lib/autocheck';
import { AppLayout, ProductGrid } from '../../components/layout';
import { GenericButton } from '../../components/shared';

const propertiesMap = [
  { key: 'fuelType', label: 'Fuel Type' },
  { key: 'transmission', label: 'Transmission' },
  { key: 'engineType', label: 'Engine Type' },
  { key: 'interiorColor', label: 'Interior Color' },
  { key: 'exteriorColor', label: 'Exterior Color' },
  { key: 'vin', label: 'VIN' },
  { key: 'vehicleId', label: 'Vehicle ID' },
] as { key: keyof CarDetails; label: string }[];

const CarViewer = () => {
  const router = useRouter();
  const [carData, setCarData] = useState<CarDetails | null>(null);
  const [carMedia, setCarMedia] = useState<CarMedia[] | null>([]);
  const { id: carId } = router.query;

  const loadCar = useCallback(async () => {
    if (carId) {
      try {
        setCarData(await getCarData({ carId: carId as string }));
        setCarMedia(await getCarMedia({ carId: carId as string }));
      } catch (err) {
        console.log(err);
      }
    }
  }, [carId, setCarData]);

  useEffect(() => {
    loadCar();
  }, [carId]);

  return (
    <AppLayout>
      <main className='py-16'>
        <div className='container mx-auto'>
          <div className='grid grid-cols-3'>
            <div className='col-span-2'>
              <ProductGrid cols={1}>
                <div>
                  <div className='product-image mb-4'>
                    <Carousel thumbWidth={50}>
                      {carMedia?.map((media, index) => (
                        <div key={index}>
                          {media?.url && (
                            <img src={media?.url} className='w-100 h-auto' />
                          )}
                        </div>
                      ))}
                    </Carousel>
                  </div>
                  <div className='grid grid-cols-3 mb-6'>
                    <div className='col-span-2'>
                      <h3 className='text-2xl font-bold mb-2 text-gray-600'>
                        {carData?.carName}
                      </h3>
                      <ul className='mb-6'>
                        {carData?.mileage && (
                          <li className='inline-flex items-center pr-6'>
                            <FaTachometerAlt className='w-4 h-4' />
                            <span className='ml-2'>
                              {formatNumber(carData?.mileage)}
                            </span>
                          </li>
                        )}
                        {carData?.city && (
                          <li className='inline-flex items-center pr-6'>
                            <FaMapMarkedAlt className='w-4 h-4' />
                            <span className='ml-2'>{carData?.city}</span>
                          </li>
                        )}
                      </ul>
                    </div>
                    <div className='text-right'>
                      {carData?.marketplacePrice && (
                        <h2 className='text-3xl font-bold text-gray-600 mb-2'>
                          {formatCurrency(carData?.marketplacePrice)}
                        </h2>
                      )}
                      <GenericButton className='bg-green-600 rounded hover:bg-green-700 transition-all'>
                        Purchase
                      </GenericButton>
                    </div>
                  </div>
                  {carData && (
                    <>
                      <h3 className='text-lg font-semibold mb-2 text-gray-600'>
                        Vehicle Description
                      </h3>
                      <table className='table-auto w-full'>
                        <tbody>
                          {propertiesMap
                            .filter((property) => carData[property.key])
                            .map((property) => (
                              <tr className='border-b' key={property.key}>
                                <th className='text-left py-2'>
                                  {property.label}
                                </th>
                                <td
                                  width='20%'
                                  className='text-right py-2 capitalize'
                                >
                                  {carData[property.key]}
                                </td>
                              </tr>
                            ))}
                        </tbody>
                      </table>
                    </>
                  )}
                </div>
              </ProductGrid>
            </div>
          </div>
        </div>
      </main>
    </AppLayout>
  );
};

export default CarViewer;
