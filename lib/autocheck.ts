export type CarData = {
  id: string;
  title: string;
  imageUrl: string;
  year: number;
  city: string;
  state: string;
  marketplacePrice: number;
  marketplaceOldPrice: number;
  loanValue: number;
  hasFinancing: boolean;
  gradeScore: number;
  transmission: string;
};

export type CarDetails = {
  city: string;
  carName: string;
  fuelType: string;
  engineType: string;
  country: string;
  mileage: number;
  imageUrl: string;
  marketplacePrice: number;
  marketplaceOldPrice: number;
  loanValue: number;
  hasFinancing: boolean;
  gradeScore: number;
  transmission: string;
  insured: boolean;
  hasWarranty: boolean;
  exteriorColor: string;
  interiorColor: string;
  vin: string;
  sellingCondition: string;
  mileageUnit: string;
};

export type CarMedia = {
  createdAt: string;
  id: number;
  name: string;
  type: string;
  url: string;
};

export type MakeData = { id: number; name: string };

export const formatNumber = (numberToFormat: number) => {
  return new Intl.NumberFormat('en-US').format(numberToFormat);
};

export const formatCurrency = (numberToFormat: number) => {
  return new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'NGN',
    currencyDisplay: 'symbol',
    maximumFractionDigits: 0,
  }).format(numberToFormat);
};

export const getPopularMakes = async () => {
  const response = await fetch(
    'https://api.staging.myautochek.com/v1/inventory/make?popular=true',
  );
  if (response.status > 300) {
    console.error('There was an error trying to get the cars');
  }
  const responseJson = await response.json();
  return responseJson.makeList as MakeData[];
};

export const listCars = async ({ makeId }: { makeId?: number }) => {
  const response = await fetch(
    `https://api-prod.autochek.africa/v1/inventory/car/search${
      makeId ? '?make_id=' + makeId : ''
    }`,
  );
  if (response.status > 300) {
    console.error('There was an error trying to get the cars');
  }
  const responseJson = await response.json();
  return responseJson.result as CarData[];
};

export const getCarData = async ({ carId }: { carId?: string }) => {
  const response = await fetch(
    `https://api-prod.autochek.africa/v1/inventory/car/${carId}`,
  );
  if (response.status > 300) {
    console.error('There was an error trying to get the cars');
  }
  const responseJson = await response.json();
  return responseJson as CarDetails;
};

export const getCarMedia = async ({ carId }: { carId?: string }) => {
  const response = await fetch(
    `https://api-prod.autochek.africa/v1/inventory/car_media?carId=${carId}`,
  );
  if (response.status > 300) {
    console.error('There was an error trying to get the cars');
  }
  const responseJson = await response.json();
  return responseJson.carMediaList as CarMedia[];
};
