import { FaChevronRight } from 'react-icons/fa';

import { GenericButton, GenericInput } from './shared';

export const SidebarSection = ({
  title,
  children,
}: {
  title: string;
  children: React.ReactNode;
}) => {
  return (
    <div className='border-b pb-4 mb-4'>
      <h4 className='text-orange-600 text-lg font-medium mb-2'>{title}</h4>
      {children}
    </div>
  );
};

export const SideBar = () => {
  return (
    <div className='sidebar bg-gray-100 h-full p-8 rounded'>
      <SidebarSection title='Search here'>
        <form className='flex'>
          <GenericInput placeholder='Product name' height={8} textSize='sm' />
          <GenericButton height={8} textSize='lg' xPadding={2}>
            <FaChevronRight className='h-4 w-4' />
          </GenericButton>
        </form>
      </SidebarSection>
      <SidebarSection title='Fuel Type'>
        <ul>
          <li className='flex items-center'>
            <input type='checkbox' />
            <label className='ml-2'>Diesel</label>
          </li>
          <li className='flex items-center'>
            <input type='checkbox' />
            <label className='ml-2'>Petrol</label>
          </li>
          <li className='flex items-center'>
            <input type='checkbox' />
            <label className='ml-2'>Electric</label>
          </li>
        </ul>
      </SidebarSection>
      <SidebarSection title='Mileage'>
        <ul>
          <li>
            <a href='#'>Under 10,000</a>
          </li>
          <li>
            <a href='#'>Under 10,000</a>
          </li>
        </ul>
      </SidebarSection>
      <SidebarSection title='Selling condition'>
        <ul>
          <li className='flex items-center'>
            <input type='checkbox' />
            <label className='ml-2'>Brand new</label>
          </li>
          <li className='flex items-center'>
            <input type='checkbox' />
            <label className='ml-2'>Local used</label>
          </li>
          <li className='flex items-center'>
            <input type='checkbox' />
            <label className='ml-2'>Foreign used</label>
          </li>
        </ul>
      </SidebarSection>
    </div>
  );
};
