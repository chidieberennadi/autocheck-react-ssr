import { useRouter } from 'next/router';

export function GenericButton({
  bgColor = 'sky-600',
  textColor = 'white',
  height = 10,
  xPadding = 4,
  textSize = 'xl',
  className,
  href,
  children,
}: {
  bgColor?: string;
  textColor?: string;
  height?: number;
  xPadding?: number;
  textSize?: string;
  className?: string;
  href?: string;
  children: React.ReactNode;
}) {
  const router = useRouter();
  return (
    <button
      onClick={() => {
        if (href) router.push(href);
      }}
      type='submit'
      className={`h-${height} px-${xPadding} text-${textSize} text-${textColor} bg-${bgColor} ${
        className || ''
      }`}
    >
      <span>{children}</span>
    </button>
  );
}

export function GenericInput({
  borderColor = 'neutral-400',
  height = 10,
  placeholder = 'Enter some text',
  xPadding = 2,
  textSize = 'base',
  className,
}: {
  borderColor?: string;
  height?: number;
  placeholder?: string;
  xPadding?: number;
  className?: string;
  textSize?: string;
}) {
  return (
    <input
      type='text'
      className={`h-${height} leading-${height} border border-${borderColor} px-${xPadding} outline-none focus:border-blue-900 text-${textSize} ${
        className || ''
      }`}
      placeholder={placeholder}
    />
  );
}
