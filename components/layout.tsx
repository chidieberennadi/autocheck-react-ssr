import { useState, useCallback, useEffect } from 'react';
import Image from 'next/image';
import { FaFacebook, FaTwitter } from 'react-icons/fa';
import classNames from 'classnames';
import {
  FaMapMarker,
  FaTruck,
  FaPhone,
  FaSignInAlt,
  FaUserPlus,
  FaMobileAlt,
  FaEnvelope,
} from 'react-icons/fa';

import { GenericButton, GenericInput } from './shared';
import { formatCurrency, getPopularMakes, MakeData } from '../lib/autocheck';
import Link from 'next/link';

const PreHeaderStrip = () => {
  const navItems = [
    { icon: FaMapMarker, text: 'Select Location' },
    { icon: FaTruck, text: 'Track Order' },
    { icon: FaPhone, text: '08132656830' },
    { icon: FaSignInAlt, text: 'Login' },
    { icon: FaUserPlus, text: 'Register' },
  ];
  return (
    <div className='pre-header-strip flex items-center py-2 px-4 text-sm'>
      <p className='grow'>Offer Zone. Top Deals &amp; Discounts</p>
      <nav className='grow'>
        <ul className='list-none'>
          <div className='grid grid-cols-5 gap-0'>
            {navItems.map((item, index) => (
              <div
                className={classNames('flex justify-center items-center', {
                  'border-r border-r-white': index < navItems.length - 1,
                })}
                key={index}
              >
                <item.icon className='w-4 h-4 text-white' />
                <span className='ml-2'>{item.text}</span>
              </div>
            ))}
          </div>
        </ul>
      </nav>
    </div>
  );
};

const PageHeader = () => {
  return (
    <header>
      <div className='container mx-auto h-24 flex items-center'>
        <div className='w-36'>
          <Link href='/'>
            <img src='/img/full-color-logo-new.png' className='w-full h-auto' />
          </Link>
        </div>
        <div className='grow ml-8'>
          <ProductSearchForm />
        </div>
        <div>
          <button></button>
        </div>
      </div>
    </header>
  );
};

const ProductSearchForm = () => {
  return (
    <form className='flex'>
      <GenericInput placeholder='Search' className='grow' xPadding={2} />
      <GenericButton bgColor='orange-600' className='ml-2'>
        Search
      </GenericButton>
    </form>
  );
};

const PageNavBar = () => {
  const [popularMakes, setPopularMakes] = useState<MakeData[]>([]);
  const navItems = [
    { title: 'Home', link: '/' },
    { title: 'Cars', link: '/' },
    { title: 'About Us', link: '/' },
    { title: 'New Arrivals', link: '/' },
    { title: 'Pages', link: '/' },
    { title: 'Contact Us', link: '/' },
  ];

  const loadPopularMakes = useCallback(async () => {
    setPopularMakes(await getPopularMakes());
  }, [setPopularMakes]);

  useEffect(() => {
    loadPopularMakes();
  }, []);

  return (
    <nav className='bg-gray-200'>
      <div className='container h-12 mx-auto flex items-center'>
        <select className='border border-neutral-400 h-8 px-2'>
          {popularMakes.map((make) => (
            <option key={make.id}>{make.name}</option>
          ))}
        </select>
        <ul className='list-none p-0 m-0 ml-6'>
          {navItems.map((item, index) => (
            <li key={index} className='inline-block ml-6'>
              <Link href={item.link} title={item.title}>
                {item.title}
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </nav>
  );
};

const PageFooter = () => {
  return (
    <footer>
      <section className='bg-gray-600 text-white py-16'>
        <div className='container mx-auto'>
          <div className='grid grid-cols-4 gap-4'>
            <div>
              <h5 className='text-lg font-semibold mb-4'>Categories</h5>
              <ul>
                <li className='text-gray-300 h-8 leading-8'>Mobiles</li>
                <li className='text-gray-300 h-8 leading-8'>Computers</li>
                <li className='text-gray-300 h-8 leading-8'>TV Audio</li>
                <li className='text-gray-300 h-8 leading-8'>Smartphones</li>
                <li className='text-gray-300 h-8 leading-8'>
                  Washing Machines
                </li>
                <li className='text-gray-300 h-8 leading-8'>Refrigerators</li>
              </ul>
            </div>
            <div>
              <h5 className='text-lg font-semibold mb-4'>Quick Links</h5>
              <ul>
                <li className='text-gray-300 h-8 leading-8'>About Us</li>
                <li className='text-gray-300 h-8 leading-8'>Contact Us</li>
                <li className='text-gray-300 h-8 leading-8'>Help</li>
                <li className='text-gray-300 h-8 leading-8'>FAQs</li>
                <li className='text-gray-300 h-8 leading-8'>Terms of Use</li>
                <li className='text-gray-300 h-8 leading-8'>Privacy Policy</li>
              </ul>
            </div>
            <div>
              <h5 className='text-lg font-semibold mb-4'>Get in Touch</h5>
              <ul>
                <li className='text-gray-300 h-8 flex items-center'>
                  <FaUserPlus className='w-4 h-4 text-orange-600' />
                  <span className='ml-2'>17, Mile 12 Road, Ikeja</span>
                </li>
                <li className='text-gray-300 h-8 flex items-center'>
                  <FaMobileAlt className='w-4 h-4 text-orange-600' />
                  <span className='ml-2'>333 222 333222</span>
                </li>
                <li className='text-gray-300 h-8 flex items-center'>
                  <FaPhone className='w-4 h-4 text-orange-600' />
                  <span className='ml-2'>+234 813 242 4333 </span>
                </li>
                <li className='text-gray-300 h-8 flex items-center'>
                  <FaEnvelope className='w-4 h-4 text-orange-600' />
                  <span className='ml-2'>contact@example.com</span>
                </li>
                <li className='text-gray-300 h-8 flex items-center'>
                  <FaEnvelope className='w-4 h-4 text-orange-600' />
                  <span className='ml-2'>sales@example.com</span>
                </li>
              </ul>
            </div>
            <div>
              <h5 className='text-lg font-semibold mb-4'>Newsletter</h5>
              <p className='text-gray-300 mb-4'>
                Free delivery on your first order
              </p>
              <form className='flex mb-8'>
                <GenericInput
                  placeholder='Email Address'
                  height={8}
                  textSize='sm'
                />
                <GenericButton
                  height={8}
                  textSize='lg'
                  xPadding={2}
                  className='ml-2 bg-orange-600'
                >
                  Go
                </GenericButton>
              </form>
              <h5 className='text-lg font-semibold mb-4'>Follow us on</h5>
              <ul>
                <li className='inline-block'>
                  <FaFacebook className='w-6 h-6 text-sky-600' />
                </li>
                <li className='inline-block ml-4'>
                  <FaTwitter className='w-6 h-6 text-sky-400' />
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
    </footer>
  );
};

export const AppLayout = ({ children }: { children: React.ReactNode }) => {
  return (
    <div className='flex flex-col min-h-screen'>
      <div className='grow'>
        <PreHeaderStrip />
        <PageHeader />
        <PageNavBar />
        {children}
      </div>
      <PageFooter />
    </div>
  );
};

export const ProductGrid = ({
  children,
  title,
  cols = 3,
}: {
  children?: React.ReactNode;
  title?: string;
  cols?: number;
}) => {
  return (
    <section className='shadow-lg border border-neutral-200 bg-white mb-8 p-8 rounded'>
      {title && (
        <h3 className='text-xl text-center pb-6 font-semibold'>{title}</h3>
      )}
      <div className={`grid grid-cols-${cols} gap-x-6 gap-y-12`}>
        {children}
      </div>
    </section>
  );
};

export const SingleProduct = ({
  image,
  productPrice,
  productPreviousPrice,
  productName,
  linkUrl,
}: {
  image?: string;
  productPrice?: number;
  productPreviousPrice?: number;
  productName: string;
  linkUrl?: string;
}) => {
  const priceJump =
    !!productPreviousPrice &&
    !!productPrice &&
    productPrice > productPreviousPrice;

  return (
    <div className='text-center'>
      {image && <Image src={image} height={200} width={250} />}
      <h3 className='mt-2'>{productName}</h3>
      <h4>
        {productPrice && (
          <span
            className={classNames('font-semibold text-xl', {
              'text-gray-500': !priceJump,
              'text-red-600': priceJump,
            })}
          >
            {formatCurrency(productPrice)}
          </span>
        )}
        {priceJump && (
          <span className='ml-2 text-md text-gray-500 line-through'>
            {formatCurrency(productPreviousPrice)}
          </span>
        )}
      </h4>
      <GenericButton
        textSize='sm'
        height={8}
        xPadding={4}
        className='mt-4 rounded-sm shadow-md'
        href={linkUrl}
      >
        View Product
      </GenericButton>
    </div>
  );
};
